import os

import numpy as np
import pandas as pd
import re

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models import BranchProduct, Product, Base

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
ASSETS_DIR = os.path.join(PROJECT_DIR, "assets")
PRODUCTS_PATH = os.path.join(ASSETS_DIR, "PRODUCTS.csv")
PRICES_STOCK_PATH = os.path.join(ASSETS_DIR, "PRICES-STOCK.csv")

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
db_path = os.path.join(BASE_DIR, "wallmart.db")
db_engine = create_engine('sqlite:///'+db_path)
Base.metadata.create_all(db_engine)
db_session = sessionmaker(bind=db_engine)()

# Instance product model
db_model_product = Product
db_query_product = db_session.query(db_model_product)

# Instance BranchProduct model
db_model_branch = BranchProduct
db_query_branch = db_session.query(db_model_branch)

def cleanhtml(raw_html=''):
  cleanr = re.compile(r'<.*?>')
  cleantext = re.sub(cleanr, '', ''+raw_html)
  return cleantext

def extract_package(description):
    cleanr_d = re.compile(r'\d+[^abc]+')
    cleanr_p = re.compile(r'[^0-9]+\W')
    package = re.sub(cleanr_p, '', description or '')
    description = re.sub(cleanr_d, '', description or '')
    return {
        'package': package,
        'description': description
    }

def process_csv_files():
    products_df = pd.read_csv(filepath_or_buffer=PRODUCTS_PATH, sep="|",)
    prices_stock_df = pd.read_csv(
        filepath_or_buffer=PRICES_STOCK_PATH, sep="|",)
    avalible_p = []
    skus= []
    for product_brand in prices_stock_df.iterrows():
        if product_brand[1]['STOCK'] > 0 and product_brand[1]['BRANCH'] in ['MM','RHSM']:
            avalible_p.append({
                'SKU': product_brand[1]['SKU'],
                'BRANCH': product_brand[1]['BRANCH'],
                'PRICE': product_brand[1]['PRICE'],
                'STOCK': product_brand[1]['STOCK']
            })
            skus.append(product_brand[1]['SKU'])

    for product in products_df.iterrows():
        if product[1]["SKU"] in skus:
            description_bussy = cleanhtml(product[1]["DESCRIPTION"])
            description_bussy = extract_package(description_bussy)
            product_m = db_model_product(
                    store="Richart's",
                    barcodes=cleanhtml(f'{product[1]["BARCODES"]}'),
                    sku=cleanhtml(f'{product[1]["SKU"]}'),
                    brand=cleanhtml(f'{product[1]["BRAND"]}'),
                    name=cleanhtml(f'{product[1]["NAME"]}'),
                    description=description_bussy["description"],
                    package=description_bussy["package"] if description_bussy["package"] != None else '',
                    image_url=cleanhtml(f'{product[1]["IMAGE_URL"]}'),
                    category=''.join([cleanhtml(f'{item}').lower() for item in product[1]["CATEGORY"] +'|'+ product[1]["SUB_CATEGORY"] +'|'+ product[1]["SUB_SUB_CATEGORY"]]),
                )
            db_session.add(product_m)
            db_session.commit()
            pr = db_query_product.filter(db_model_product.sku == product[1]["SKU"])
            
            # Change for lambda
            def get_b_p(item):
                if item['SKU'] == product[1]["SKU"]:
                    return item
            branch_product = filter(get_b_p,avalible_p)
            branch_product = [x for x in branch_product][0]
            branch_product_m = db_model_branch(
                product_id=[x.__dict__ for x in pr][0]['id'],
                branch=branch_product['BRANCH'],
                stock=branch_product['STOCK'],
                price=branch_product['PRICE']
            )
            db_session.add(branch_product_m)
            db_session.commit()


if __name__ == "__main__":
    process_csv_files()
