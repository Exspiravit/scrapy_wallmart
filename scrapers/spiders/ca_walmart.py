import scrapy
from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import models
from scrapers.items import ProductItem

import os.path
import json


class CaWalmartSpider(scrapy.Spider):

    name = "ca_walmart"
    allowed_domains = ["walmart.ca"]
    start_urls = [
        "https://www.walmart.ca/en/grocery/fruits-vegetables/fruits/N-3852"]

    rules = {
        Rule(LinkExtractor(
            allow=(), restrict_xpaths=('//a[@id="loadmore"]'))),
        Rule(LinkExtractor(allow=(), restrict_xpaths=(
            '//a[@class="product-link"]')), callback='parse_item', follow=False),
    }

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
    db_path = os.path.join(BASE_DIR, "wallmart.db")
    db_engine = create_engine('sqlite:///'+db_path)
    models.Base.metadata.create_all(db_engine)
    db_session = sessionmaker(bind=db_engine)()

    # Instance product model
    db_model_product = models.Product
    db_query_product = db_session.query(db_model_product)

    # Instance BranchProduct model
    db_model_branch = models.BranchProduct
    db_query_branch = db_session.query(db_model_branch)
    # HEADERS
    headers_ = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'es-419,es;q=0.9',
        'cache-control': 'max-age=0',
        'referer': 'https://www.walmart.ca/en/grocery/fruits-vegetables/fruits/N-3852',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'
    }

    # wallmart require cookies for scrapy because if dont have location redirect to flyer view.
    # RAW COOKIES
    raw_cookies = 'walmart.shippingPostalCode=P7B3Z7; defaultNearestStoreId=3124; zone=9; deliveryCatchment=3124; walmart.csrf=2fc7b780afe77bd015d6a80b; wmt.c=0; vtc=edLbG2pZipZYK5ZKk_rdic; TS011fb5f6=01c5a4e2f9712ef3c3e019958b4038abd276ce432b5ae3db247889b800ddb9fbbb7b8ae80af381eba6f10993f9dbc969402f280442; TS0175e29f=01c5a4e2f9712ef3c3e019958b4038abd276ce432b5ae3db247889b800ddb9fbbb7b8ae80af381eba6f10993f9dbc969402f280442; userSegment=40-percent; wmt.breakpoint=d; TBV=7; rxVisitor=159077687778383BNM1LDLMPVL73NDQSC3O9GFOUEC2EJ; dtSa=-; walmart.id=30cd219d-fcc8-4987-c9a6-c4ac9ae9087e; usrState=1; previousBreakpoint=desktop; AMCVS_C4C6370453309C960A490D44%40AdobeOrg=1; walmart.locale=en; walmart.nearestPostalCode=P7B3Z7; og_session_id=af0a84f8847311e3b233bc764e1107f2.649934.1590776881; og_session_id_conf=af0a84f8847311e3b233bc764e1107f2.649934.1590776881; og_autoship=0; dtCookie=22$22KBGJ1TEU336CBMJLKD9BVH9J9DN13A|5b6d58542e634882|0; walmart.nearestLatLng="48.4120872,-89.2413988"; rxVisitor=159077687778383BNM1LDLMPVL73NDQSC3O9GFOUEC2EJ; dtLatC=160; dtSa=true%7CS%7C-1%7C-%7C-%7C1590777787215%7C176877775_10%7Chttps%3A%2F%2Fwww.walmart.ca%2Fen%2Fgrocery%2Ffruits-vegetables%2Ffruits%2FN-3852%7CBuy%20Fresh%20Fruit%20Online%20%5Ep%20Walmart%20Canada%7C1590776884577%7C%7C; headerType=grocery; DYN_USER_ID=7188dfae-c456-4d98-91cc-24415933bad7; WM_SEC.AUTH_TOKEN=MTAyOTYyMDE4eGZVkFORZWAAC/xOFoc5evcra4pt6ZxrLX6xEohDa4pdY0F0mOmPGexElMbx8wstx9wRu3MQYA6FNMGcUqaDYZMrdWYCmZWs6hOqMVLXLa/cozcNK9f84SiapWOq4iI8j8OFN4dileb20bpDLeCIlSFd/Hsc7bnSe4+TLU2zbj3uPJV0nC6mqJfVqKogkF9YNJdqQ/6TMz7IC9+Z2BCNTdyNHN4Za06JehpSVAy5S87b/SoGFgAYL9DGZ8K45WCXb/Ew67/GsLtdlJHpe1JgEP6pkP+hGnWNfvdHrC+h8McFC086NPwTTVBheCUJWi0Dhq+UZbj5q+k6nueNLB6rLyL/EDudrfMNqS2gKuSH6Ml6opZ2AsXJOHFPPdtSQULR6/vzIBfzEb7tXhj9nARyAQ==; LT=1590778244277; BVImplmain_site=2036; NEXT_GEN.ENABLED=1; cartId=87b95000-018f-4183-a51f-ae8f21a8c5d0; authDuration={"lat":"1590778480956000","lt":"1590778480956000"}; DYN_USER_ID.ro=7188dfae-c456-4d98-91cc-24415933bad7; cookiePolicy=true; enableHTTPS=1; ENV=ak-cdc-prod; bstc=T2TisRZO4W31BjqOabiwYw; xpa=2lwWQ|34JTv|HeXkG|LVSOt|MZ9tt|NOECn|Nb72k|YuWN8|_vY-K|abOKO|bLgD2|eP05T|gbvoH|iZQfb|jeBOs|lZnE7|mOlOu|or_xY|oygXn|pXPz1|qvGc6|sGGbM|ssJZF|t5K4Y|wR_Xy|xnDWd|yI7_k; exp-ck=HeXkG1MZ9tt1NOECn2Nb72k4YuWN85abOKOIbLgD21eP05T1gbvoH1iZQfb1lZnE76or_xY4oygXn2pXPz13sGGbM4ssJZF7t5K4Y1wR_Xy1xnDWd1yI7_k1; TS01f4281b=01c5a4e2f911add84e7868a02bafe8d7c27df43eeb3d7899c6ce91a54679339ce2f3e3c9e8d28d51ed1d22a6a1e5b6cc79b9fee470; akaau_P1=1590807410~id=1226ee6176b3004ae3baefc8d8c67a4e; xpm=1%2B1590805610%2BedLbG2pZipZYK5ZKk_rdic~%2B0; rxvt=1590807421509|1590805611231; dtPC=22$205611213_248h1p22$205619955_614h1p22$205621501_251h1vDHKKXSEUMWBBOKPMSMTJPLZSLKIJPBLJ-0; AMCV_C4C6370453309C960A490D44%40AdobeOrg=-408604571%7CMCIDTS%7C18412%7CMCMID%7C43627577849897110018331561788342724448%7CMCAID%7CNONE%7CMCOPTOUT-1590812821s%7CNONE%7CvVersion%7C4.6.0; seqnum=10'

    cookies = {}
    for cookie in raw_cookies.split('; '):
        try:
            key = cookie.split('=')[0]
            val = cookie.split('=')[1]
            cookies[key] = val

        except expression as identifier:
            pass

    def parse(self, response):

        url_ = 'https://www.walmart.ca/en/grocery/fruits-vegetables/fruits/N-3852'

        req = scrapy.Request(
            url_,
            cookies=self.cookies,
            headers=self.headers_,
            callback=self.parse_product_pages
        )

        yield req

    def parse_product_pages(self, response):
        next_page = response.xpath('//a[@id="loadmore"]/@href').get()
        products = response.xpath('//a[@class="product-link"]/@href').getall()
        products = ['https://www.' +
                    self.allowed_domains[0] + x for x in products]
        # print(products)
        for product_url in products:
            req = scrapy.Request(
                product_url,
                cookies=self.cookies,
                headers=self.headers_,
                callback=self.parse_item
            )
            yield req
        if next_page != None:
            req = scrapy.Request(
                'https://www.' + self.allowed_domains[0] + next_page,
                cookies=self.cookies,
                headers=self.headers_,
                callback=self.parse_product_pages
            )

            yield req

    def parse_item(self, response):
        product_item = ProductItem()

        prod_scrip = json.loads(response.body.decode("utf-8").split('__PRELOADED_STATE__=')
                                [1].split(';</script>')[0])

        product_item['store'] = 'Walmart'
        product_item['barcodes'] = prod_scrip['entities']['skus'][prod_scrip['product']
                                                                  ['activeSkuId']]['upc']
        product_item['sku'] = prod_scrip['product']['activeSkuId']
        product_item['brand'] = prod_scrip['entities']['skus'][prod_scrip['product']
                                                               ['activeSkuId']]['brand']['name']
        product_item['name'] = response.xpath(
            '//h1[@data-automation="product-title"]/text()').get()
        product_item['description'] = prod_scrip['entities']['skus'][prod_scrip['product']
                                                                     ['activeSkuId']]['longDescription']
        product_item['package'] = response.xpath(
            '//div[@data-automation="product-variable-weight"]/text()').get()
        product_item['image_url'] = response.xpath(
            '//div[@role="presentation"]/img/@src').getall()
        # product_item['branch'] = response.css('').get()
        # product_item['stock'] = response.css('').get()
        # product_item['price'] = response.css('').get()

        product = self.db_model_product(
            store=product_item['store'],
            barcodes=','.join(product_item['barcodes']),
            sku=product_item['sku'],
            brand=product_item['brand'],
            name=product_item['name'],
            description=product_item['description'],
            package=product_item['package'],
            image_url=','.join(product_item['image_url']),
            category='>'.join(response.xpath(
                '//nav[@role="navigation"]/ol/li/a/text()').getall()),
            url=response.url,
        )
        self.db_session.add(product)
        self.db_session.commit()
        yield product_item
